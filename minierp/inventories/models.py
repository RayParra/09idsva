from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Base(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
    slug = models.SlugField(default="Generic")
    status = models.BooleanField(default=True)

    class Meta:
        abstract = True


class Category(Base):
    description = models.CharField(max_length=64, default="Generic")


    def __str__(self):
        return self.description



class SubCategory(Base):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    description = models.CharField(max_length=128, default="Generic")

    def __str__(self):
        return f"{self.category.description} - {self.description}"


mesure_choices = (
    ("Size", "SZ"),
    ("Unit", "UN"),
    ("Package", "PK"),
)

class Product(Base):
    subcategory = models.ForeignKey(SubCategory, on_delete=models.CASCADE)
    name = models.CharField(max_length=64, default="Generic")
    mesure = models.CharField(max_length=32, default="unit", choices=mesure_choices)
    stock = models.IntegerField(default=0)
    price = models.FloatField(default=0.0)
    slug = models.SlugField(default="generic")
    status = models.BooleanField()

    def __str__(self):
        return self.name