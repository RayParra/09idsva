# Generated by Django 4.0.6 on 2022-07-07 02:36

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('slug', models.SlugField(default='Generic')),
                ('status', models.BooleanField(default=True)),
                ('description', models.CharField(default='Generic', max_length=64)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='SubCategory',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('slug', models.SlugField(default='Generic')),
                ('status', models.BooleanField(default=True)),
                ('description', models.CharField(default='Generic', max_length=128)),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='inventories.category')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('name', models.CharField(default='Generic', max_length=64)),
                ('mesure', models.CharField(choices=[('Size', 'SZ'), ('Unit', 'UN'), ('Package', 'PK')], default='unit', max_length=32)),
                ('stock', models.IntegerField(default=0)),
                ('price', models.FloatField(default=0.0)),
                ('slug', models.SlugField(default='generic')),
                ('status', models.BooleanField()),
                ('subcategory', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='inventories.subcategory')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
