from django.urls import path

from inventories import views


app_name = "inv"


urlpatterns = [
    path("v1/inv/list/", views.CategoryList.as_view(), name="inv_list"),
    path("v2/inv/list_create/", views.CategoryListCreate.as_view(), name="list_create_category"),
    path("v2/inv/detail_update/<int:pk>/", views.CategoryRetrieveUpdateDestroy.as_view(), name="detail_update"),
    path("v2/inv/list_create_subcategory/", views.SubCategoryListCreate.as_view(), name="list_create_subcategory"),
    path("v2/inv/detail_update_delete_subcategory/<int:pk>/", views.SubCategoryRetrieveUpdateDestroy.as_view(), name="detail_update_subcategory"),
    path("v2/inv/list_create_product/", views.ProductListCreate.as_view(), name="list_create_product"),
    path("v2/inv/detail_update_delete_product/<int:pk>/", views.ProductRetrieveUpdateDestroy.as_view(), name="detail_update_product"),
]


