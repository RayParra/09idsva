from unicodedata import category
from rest_framework import serializers
#from django.contrib.auth.models import User

from .models import Category, SubCategory, Product
#from inventories import models

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = "__all__"


class SubCategorySerializer(serializers.ModelSerializer):
    category = CategorySerializer()
    class Meta:
        model = SubCategory
        fields = "__all__"



class ProductSerializer(serializers.ModelSerializer):
    subcategory = SubCategorySerializer()
    class Meta:
        model = Product
        fields = "__all__"