from django.contrib import admin

# Register your models here.
from inventories.models import Category, SubCategory, Product



@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ["description", "user", "timestamp"]



@admin.register(SubCategory)
class SubCategoryAdmin(admin.ModelAdmin):
    list_display = ["description", "category", "user", "timestamp"]


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ["name", "subcategory", "user", "stock", "timestamp"]