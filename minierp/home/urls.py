from django.urls import path
from rest_framework.authtoken import views as auth_views

from home import views

app_name = "home"


urlpatterns = [
    path('', views.index, name="index"),
    path("v1/user_list/", views.UserList.as_view(), name="user_list"),
    path("v1/list_empleados/", views.EmpleadoView.as_view(), name="list_empleados"),
    path("v1/create_empleados/", views.EmpleadoListCreate.as_view(), name="create_empleado"),
    path("v2/create_user/", views.CreateUser.as_view(), name="create_user"),
    path("v2/detail_update_delete/<int:pk>/", views.EmpleadoRetrieveUpdateDestroy.as_view(), name="detail_update_delete"),
    path("login/", views.LoginView.as_view(), name="login"),
    path("login-drf/", auth_views.obtain_auth_token, name="login-drf"),
]
