import email
from sqlite3 import Timestamp
from django.db import models
from django.contrib.auth.models import User
from django.conf import settings

# Create your models here.

 
     
class OwnerModel(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
     
    class Meta:
        abstract=True


deparment_choices = (
    ('Sys', 'Sistemas'),
    ('Inv', 'Inventaries'),
    ('Byr', 'Buyers'),
    ('Ad', 'Admin'),
)

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bio = models.CharField(max_length=256, default="Im an User")
    avatar = models.ImageField(upload_to="img/avatars")
    department = models.CharField(max_length=32, choices=deparment_choices)

    def __str__(self):
        return self.user.first_name



class Empleado(OwnerModel, models.Model):
    name = models.CharField(max_length=32, default="empleado generico")
    email = models.CharField(max_length=32, default="email@email.email")
    timestamp = models.DateTimeField(auto_now_add=True)
    status = models.BooleanField(default=True)
    slug = models.SlugField(max_length=32, default="slug_generico")
    cargo = models.CharField(max_length=32, default="area general")


    def __str__(self):
        return self.name