from django.shortcuts import render

from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from home.serializers import UserSerializer, EmpleadoSerializer
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from .models import Empleado
from .permissions import IsOwner
from rest_framework.permissions import IsAuthenticated
# Create your views here.


def index(request):
    context = {
        
    }
    return render(request, "home/index.html", context)


class LoginView(APIView):
    permission_clases = ()
    def post(self, request):
        username = request.data.get("username")
        password = request.data.get("password")
        user = authenticate(username=username, password=password)
        if user:
            return Response({"token": user.auth_token.key})
        else:
            return Response({"error": "Wrong Credentials"}, status=status.HTTP_400_BAD_REQUEST)





class CreateUser(generics.CreateAPIView):
    serializer_class = UserSerializer
    authentication_clases = ()
    permission_clases = ()


class UserList(APIView):
    serializer_class = UserSerializer

    def get(self,request):
        users = User.objects.all()
        data = UserSerializer(users, many=True).data
        return Response(data)




class EmpleadoView(APIView):
    serializer_class = EmpleadoSerializer
    permission_classes = ([IsAuthenticated, IsOwner])

    def get(self, request):
        empleados = Empleado.objects.all()
        data = EmpleadoSerializer(empleados, many=True).data
        return Response(data)


class EmpleadoListCreate(generics.ListCreateAPIView):
    queryset = Empleado.objects.all()
    serializer_class = EmpleadoSerializer


class EmpleadoRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Empleado.objects.all()
    serializer_class = EmpleadoSerializer
    permission_classes = ([IsAuthenticated, IsOwner])
