from django.contrib import admin

# Register your models here.
from home.models import Empleado, Profile


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ["user", "department", "bio", "avatar"]


@admin.register(Empleado)
class EmpleadoAdmin(admin.ModelAdmin):
    list_display = ["name", "email", "status", "timestamp"]